from os import environ

import databases


TESTING = environ.get("TESTING")

if TESTING:
    # Use separate DB for tests
    DB_NAME = "async-test"
    TEST_SQLALCHEMY_DATABASE_URL = (
        f"sqlite:///./database/test.db"
    )
    database = databases.Database(TEST_SQLALCHEMY_DATABASE_URL)
else:
    DB_NAME = "async-test"
    SQLALCHEMY_DATABASE_URL = (
        f"sqlite:///./database/test.db"
    )
    database = databases.Database(SQLALCHEMY_DATABASE_URL)