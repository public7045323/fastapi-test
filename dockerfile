FROM python:3.10

RUN mkdir /fastapi

WORKDIR /fastapi

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

LABEL version="1.0" maintainer="NicKSarD <fogroot@gmail.com>"

CMD gunicorn my_main:app --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind=0.0.0.0:8080