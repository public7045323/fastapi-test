import uvicorn
from fastapi import FastAPI
from models import userModel
from database.connection import engine
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from routes import userRoute
# config DB
userModel.Base.metadata.create_all(bind=engine)

# instance 
app=FastAPI()

# config static and templates
# app.mount("/static/",StaticFiles(directory="static",html=True))
# templates=Jinja2Templates(directory="templates/")

templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")


# config routes
app.include_router(userRoute.userRouter)



if __name__ == "__main__":
    uvicorn.run("my_main:app",reload=True)
