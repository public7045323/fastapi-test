from sqlalchemy.orm import Session
from sqlalchemy import select, desc
from models.userModel import User, Salary
from security.authSecurity import verify_password
from database.connection import engine2, engine


def create_user(db: Session, signup:User) -> bool:
        try:
            db.add(signup)
            db.commit()
        except:
            return False
        return True


def get_user_by_username(db: Session, username:str):
    return db.query(User).filter(User.username==username).first()


def authenticate_user(db: Session, username: str, password: str):
    user = get_user_by_username(db=db, username=username)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user


def get_salary(username):
    try:
        conn = engine2.connect() 
        sel = select(Salary).where(Salary.c.username == username).order_by(desc("date")) 
        res = conn.execute(sel)
        # print(res)
        res_final = []
        for item in res:
            # print(username)
            res_final.append({'name': item[1], 'salary': item[2], 'date': item[3].strftime("%d-%m-%Y")})
        return res_final
    except Exception as ex:
        print('error', ex)
        return 'No salary.'
