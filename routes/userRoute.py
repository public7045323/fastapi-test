from fastapi import Depends,APIRouter,Request,Response,status
from sqlalchemy.orm import Session
from fastapi.templating import Jinja2Templates
from database.connection import get_db
# from fastapi.security import OAuth2PasswordRequestForm
from security.oauth2 import OAuth2PasswordRequestForm, OAuth2PasswordRequestFormIn
from controller import userController
from models.userModel import User
from schema.userSchema import Token
from security import authSecurity
from datetime import timedelta, datetime
from starlette.responses import RedirectResponse


# instance
userRouter=APIRouter()
templates=Jinja2Templates(directory="templates/")


@userRouter.get("/")
@userRouter.get("/index")
def get_index(request:Request):
    my_data = {'title': 'Main'}
    return templates.TemplateResponse("front/home/index.html", {"request":request, 'my_data': my_data})


@userRouter.get("/signup")
def get_signup(request:Request):
    my_data = {'title': 'Signup'}
    return templates.TemplateResponse("auth/signup.html", {"request":request, 'my_data': my_data})


@userRouter.get("/signin")
def get_signup(request:Request):
    my_data = {'title': 'Enter'}
    return templates.TemplateResponse("auth/signin.html", {"request":request, 'my_data': my_data})


@userRouter.get("/salary")
def get_salary(request:Request, current_user: User=Depends(authSecurity.get_current_user)):
    next_salary_date = ((datetime.now().replace(day=1) + timedelta(days=32)).replace(day=1).strftime("%d-%m-%Y"))
    my_data = {'title': 'Salary'}
    salary = userController.get_salary(current_user)
    return templates.TemplateResponse("front/salary.html", 
                                    {"request": request, "user": current_user, 'my_data': my_data, 'salary': salary, 
                                    'next_salary_date': next_salary_date})



@userRouter.post("/signup")
def create_new_user(request:Request, db:Session=Depends(get_db), form_data:OAuth2PasswordRequestForm=Depends()):
    my_data = {'title': 'Signup'}
    db_user = userController.get_user_by_username(db=db, username=form_data.username)
    if db_user:
        error="User already exist"
        return templates.TemplateResponse("auth/signup.html", {"request":request, "error":error, 'my_data': my_data},status_code=301)
    else:
        new_user=User(email=form_data.email, username=form_data.username, password=authSecurity.get_password_hash(form_data.password))
        user=userController.create_user(db=db, signup=new_user)
        return templates.TemplateResponse("auth/signin.html", {"request":request, "user":user, 'my_data': my_data}, status_code=200)


@userRouter.post("/signin", response_model=Token)
def login_for_access_token(response:Response, request:Request, form_data: OAuth2PasswordRequestFormIn = Depends(), db: Session = Depends(get_db)): 
    user = userController.authenticate_user(
        db=db,
        username=form_data.username,
        password=form_data.password
    )
    if not user:
        error =f"User not found: {form_data.username}"
        my_data = {'title': 'Salary'}
        return templates.TemplateResponse('auth/signin.html', {"error":error, "request":request, 'my_data': my_data}, status_code=301)

    access_token_expires = timedelta(minutes=authSecurity.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = authSecurity.create_access_token(
        data={"sub": user.username,"role":user.is_Admin}, expires_delta=access_token_expires
    )
    response = RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)

    # to save token in cookie
    response.set_cookie(key="access_token",value=f"Bearer {access_token}", httponly=True) 
    return response





@userRouter.get("/signout", response_model=Token)
def get_signout(request:Request):
    response = RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)
    response.delete_cookie(key="access_token") 
    return response
