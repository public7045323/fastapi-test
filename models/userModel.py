from sqlalchemy import Boolean, Column, Integer, String, TIMESTAMP, MetaData, Table
from database.connection import Base
from datetime import datetime


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    username = Column(String, unique=True, index=True)
    password= Column(String)
    is_Admin = Column(Boolean, default=False)
    is_active = Column(Boolean, default=True)


# class Salary(Base):
#     __tablename__ = "salary"

#     id = Column(Integer, primary_key=True, index=True)
#     username = Column(String)
#     salary = Column(Integer, index=True)
#     date = Column(TIMESTAMP, default=datetime.utcnow)

metadata = MetaData()
Salary = Table(
    'salary',
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("username", String, nullable=False),
    Column("salary", Integer),
    Column("date", TIMESTAMP, default=datetime.utcnow),
)